# crx-youtube-vid-like-pct
Calculates percent rating of likes compared to total views by querying DOM when youtube video page has loaded, and displays percentage score calculated by injecting html into existing page components. 

This script runs succuessfully when executed from the chrome console, but the main DOM query returns undefined when running from chrome extension. 
Feel free to add suggestions or thoughts on how to access page objects/methods which are currently not accessible from the chrome extension script file, I believe a work-around exists, but more research would be needed to determine possible solutions and implement. Scripting efforts are currently a lower priority for me at this moment so I can focus on full stack application frameworks, but I would like to iterate on this project at some point. 
